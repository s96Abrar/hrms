# House Rental Management System

A management system where a tenant can rent a house and an owner can show his/her house ads.

This repository contains all the data about the project.
It contains-

* [Project Scenario](./Docs/Scenario/Final/HouseRentalManagementSystem-Scenario.pdf)
* [Activity Diagram](./Docs/Analysis/Use Case/Temp/Use-case-Actvity-Diagram-temp.pdf)
* [Use Case Description](./Docs/Analysis/Use Case/Final/HRMS-Use-Case-Description-modified.pdf)
* [Use Case Diagram](./Docs/Analysis/Use Case/HRMS - Use Case 2.svg)
* [Traceability Matrix](./Docs/Traceability matrix/Final/HRMS - Traceabillity Matrix.pdf)
* [SRS](./Docs/SRS/Final/HRMS - Final SRS.pdf)
* [Proposed ER Diagram](./Docs/Proposed Database Design/Design/ERDiagram.png)